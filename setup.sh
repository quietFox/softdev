#!/bin/bash

virtualenv -p python3 FGwarband

. FGwarband/bin/activate

pip3 install Flask==0.11.1

python3 -m unittest tests/framework_test.py -b

python3 framework.py
