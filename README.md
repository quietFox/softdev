# README #

Greetings,

This is a repository that stores attempts to fix a series of issues with a provided piece of code on Python and HTML and JavaScript.
The code creates a Flask web application for the managing of teams (warbands) for an fictitious tabletop game.

Main code on "framework.py" that creates the Flask application that validates the creation and edition of warbands such that
they follow the fictitious game's rules. This will be indistinctly referred to as 'backend' as well. This is where the bulk
of changes are located in.

### How do I get set up? ###

The application requires installation of Flask in order to run locally, as well as some web browser (it has only been tested on
Firefox and Google Chrome - where it works properly). 

At the moment, there is a bash script that sets up a virtual environment called 'FGwarband' which installs Flask for python3, and 
hence python3 must be used when calling the program. 

$ bash setup.sh

### Who do I talk to? ###

Contact via email: Andres Cathey through s1671778@sms.ed.ac.uk
