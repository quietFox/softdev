import unittest
import json
import os
from framework import app


class FlaskFrameworkTests(unittest.TestCase):
    def setUp(self):
        """
        self.tester is an instance of the framework app.
        """
        self.tester = app.test_client()

    # Ensure index.html loads correctly.
    def test_index_page_text(self):
        response = self.tester.get('/', content_type='html/text')
        self.assertTrue(b'FG Warband Manager' in response.data)

    # Ensure index.html loads correctly.
    def test_index_page_code(self):
        response = self.tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    # Ensure 'New Warband' page loads correctly.
    def test_bandlist_page_name(self):
        response = self.tester.get('/new', content_type='html/text')
        self.assertTrue(b'Ship Crew Editor' in response.data)

    # Ensure 'New Warband' page loads correctly.
    def test_bandlist_page_code(self):
        response = self.tester.get('/new', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    # Ensure 'Edit Warband' page loads correctly.
    def test_editband_page_text(self):
        response = self.tester.get('/edit', content_type='html/text')
        self.assertTrue(b'Bands' in response.data)

    # Ensure 'Edit Warband' page loads correctly.
    def test_editband_page_code(self):
        response = self.tester.get('/edit', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    # Ensure creating a band with only a captain has not errors.
    def test_create_band(self):
        response = self.tester.post('/new', data=dict(bandname="test0", capspec="Marksman",
                                                      capskill="Pierce", capweap="Blaster", troops=json.dumps(dict())))
        self.assertEqual(response.status_code, 201)
        os.remove('bands/test0')

    # Ensure previously created band has the proper name.
    def test_edit_created_band_name(self):
        self.tester.post('/new', data=dict(bandname="test1", capspec="Marksman",
                                           capskill="Pierce", capweap="Blaster", troops=json.dumps(dict())))
        response = self.tester.get('/edit/test1')
        self.assertIn(b'test', response.data)
        os.remove('bands/test1')

    # Ensure previously created band has the proper gold.
    def test_edit_created_band_gold(self):
        self.tester.post('/new', data=dict(bandname="test2", capspec="Marksman",
                                           capskill="Pierce", capweap="Blaster", troops=json.dumps(dict())))
        response = self.tester.get('/edit/test2')
        self.assertIn(b'495', response.data)
        os.remove('bands/test2')

    # Ensure previously created band has the proper gold.
    def test_edit_created_band_code(self):
        self.tester.post('/new', data=dict(bandname="test3", capspec="Marksman",
                                           capskill="Pierce", capweap="Blaster", troops=json.dumps(dict())))
        response = self.tester.get('/edit/test3')
        self.assertEqual(response.status_code, 200)
        os.remove('bands/test3')

    # Ensure previously created band can be posted.
    def test_post_created_band_code(self):
        self.tester.post('/new', data=dict(bandname="test4", capspec="Marksman",
                                           capskill="Pierce", capweap="Blaster", troops=json.dumps(dict())))
        response = self.tester.post('/edit/test4', data=dict(bandname="test4", capspec="Marksman",
                                                            capskill=json.dumps(["Pierce"]), capweap="Blaster",
                                                            troops=json.dumps([]), capmove="5", capfight="2",
                                                            capshoot="2", capshield="12", capmorale="4",
                                                            caphealth="12", capexperience="10"))
        self.assertEqual(response.status_code, 200)
        os.remove('bands/test4')

    def test_post_created_band_gold(self):
        self.tester.post('/new', data=dict(bandname="test5", capspec="Marksman",
                                           capskill="Pierce", capweap="Blaster", troops=json.dumps(dict())))
        self.tester.post('/edit/test5', data=dict(bandname="test5", capspec="Marksman",
                                                            capskill=json.dumps(["Pierce"]), capweap="Blaster",
                                                            troops=json.dumps([]), capmove="5", capfight="2",
                                                            capshoot="2", capshield="12", capmorale="4",
                                                            caphealth="12", capexperience="10"))
        response = self.tester.get('/edit/test5')
        self.assertIn(b'495', response.data)  # Check no double counting in weapons.
        os.remove('bands/test5')

    # Ensure creating a band with a captain and an ensign has no errors.
    def test_create_ensign_band(self):
        response = self.tester.post('/new', data=dict(bandname="test_ens0", capspec="Melee",
                                                      capskill="Block", capweap="Blaster",
                                                      troops=json.dumps(['Empty', 'Empty', 'Empty',
                                                                         'Empty', 'Empty', 'Empty',
                                                                         'Empty', 'Empty']),
                                                      hasensign=True, ensspec="Marksman", ensskill="Aim",
                                                      ensweap="Blade"))
        self.assertEqual(response.status_code, 201)
        os.remove('bands/test_ens0')

    # Ensure you can access the previously created band.
    def test_edit_created_ensign_band_code(self):
        self.tester.post('/new', data=dict(bandname="test_ens1", capspec="Melee", capskill="Block", capweap="Blaster",
                                           troops=json.dumps(['Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty']),
                                           hasensign=True, ensspec="Marksman", ensskill="Aim", ensweap="Blade"))
        response = self.tester.get('/edit/test_ens1')
        self.assertEqual(response.status_code, 200)
        os.remove('bands/test_ens1')

    # Ensure previously created band has the proper name.
    def test_edit_created_ensign_band_name(self):
        self.tester.post('/new', data=dict(bandname="test_ens2", capspec="Melee", capskill="Block", capweap="Blaster",
                                           troops=json.dumps(['Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty']),
                                           hasensign=True, ensspec="Marksman", ensskill="Aim", ensweap="Blade"))
        response = self.tester.get('/edit/test_ens2')
        self.assertIn(b'test_ens', response.data)
        os.remove('bands/test_ens2')

    # Ensure previously created band has the remaining gold 500-250-5-3=242.
    def test_edit_created_ensign_band_gold(self):
        self.tester.post('/new', data=dict(bandname="test_ens3", capspec="Melee", capskill="Block", capweap="Blaster",
                                           troops=json.dumps(['Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty']),
                                           hasensign=True, ensspec="Marksman", ensskill="Aim", ensweap="Blade"))
        response = self.tester.get('/edit/test_ens3')
        self.assertIn(b'242', response.data)
        os.remove('bands/test_ens3')

    # Ensure previously created band with ensign can be posted.
    def test_post_created_ensign_band_code(self):
        self.tester.post('/new', data=dict(bandname="test_ens4", capspec="Melee", capskill="Block", capweap="Blaster",
                                           troops=json.dumps(['Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty']),
                                           hasensign=True, ensspec="Marksman", ensskill="Aim", ensweap="Blade"))
        response = self.tester.post('/edit/test_ens4', data=dict(bandname="test_ens4", capspec="Melee",
                                                                capskill=json.dumps(["Block"]), capweap="Blaster",
                                                                troops=json.dumps(['Empty', 'Empty', 'Empty', 'Empty',
                                                                                   'Empty', 'Empty', 'Empty', 'Empty']),
                                                                capmove="5", capfight="2",
                                                                capshoot="2", capshield="12", capmorale="4",
                                                                caphealth="12", capexperience="10", hasensign=True,
                                                                ensspec="Marksman", ensskill=json.dumps(["Aim"]),
                                                                ensmove="7", ensfight="0", ensshoot="-1",
                                                                ensshield="10",
                                                                ensmorale="2", enshealth="8", ensexperience="0",
                                                                ensweap="Blade"))
        self.assertEqual(response.status_code, 200)
        os.remove('bands/test_ens4')

    def test_post_created_ensign_band_gold(self):
        self.tester.post('/new', data=dict(bandname="test_ens5", capspec="Melee", capskill="Block", capweap="Blaster",
                                           troops=json.dumps(['Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty']),
                                           hasensign=True, ensspec="Marksman", ensskill="Aim", ensweap="Blade"))
        self.tester.post('/edit/test_ens5', data=dict(bandname="test_ens5", capspec="Marksman",
                                                                capskill=json.dumps(["Block"]), capweap="Blaster",
                                                                troops=json.dumps(['Empty', 'Empty', 'Empty', 'Empty',
                                                                                   'Empty', 'Empty', 'Empty', 'Empty']),
                                                                capmove="5", capfight="2",
                                                                capshoot="2", capshield="12", capmorale="4",
                                                                caphealth="12", capexperience="10", hasensign=True,
                                                                ensspec="Marksman", ensskill=json.dumps(["Aim"]),
                                                                ensmove="7", ensfight="0", ensshoot="-1",
                                                                ensshield="10",
                                                                ensmorale="2", enshealth="8", ensexperience="0",
                                                                ensweap="Blade"))
        response = self.tester.get('/edit/test_ens5')
        self.assertIn(b'242', response.data)  # Check no double counting in weapons.
        os.remove('bands/test_ens5')

    def test_post_created_ensign_band_crowded(self):
        self.tester.post('/new', data=dict(bandname="test_ens6", capspec="Melee", capskill="Block", capweap="Blaster",
                                           troops=json.dumps(['Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty', 'Empty',
                                                              'Empty', 'Empty']),
                                           hasensign=True, ensspec="Marksman", ensskill="Aim", ensweap="Blade"))
        response = self.tester.post('/edit/test_ens6', data=dict(bandname="test_ens6", capspec="Melee",
                                                                capskill=json.dumps(["Block"]), capweap="Blaster",
                                                                troops=json.dumps(
                                                                    ['Lackey', 'Lackey', 'Lackey', 'Lackey',
                                                                     'Lackey', 'Lackey', 'Lackey', 'Lackey', 'Lackey']),
                                                                capmove="5", capfight="2",
                                                                capshoot="2", capshield="12", capmorale="4",
                                                                caphealth="12", capexperience="10", hasensign=True,
                                                                ensspec="Marksman", ensskill=json.dumps(["Aim"]),
                                                                ensmove="7", ensfight="0", ensshoot="-1",
                                                                ensshield="10",
                                                                ensmorale="2", enshealth="8", ensexperience="0",
                                                                ensweap="Blade"))
        self.assertEqual(response.status_code, 400)  # Checks more than 10 members is caught.
        os.remove('bands/test_ens6')

    def test_create_troop_band(self):
        response = self.tester.post('/new', data=dict(bandname="test_troop0", capspec="Marksman",
                                                      capskill="Pierce", capweap="Blaster",
                                                      troops=json.dumps(['Lackey', 'Lackey', 'Lackey',
                                                                         'Lackey', 'Lackey', 'Lackey',
                                                                         'Lackey', 'Lackey', 'Lackey'])))
        self.assertEqual(response.status_code, 201)
        os.remove('bands/test_troop0')

    def test_remove_troops_created_troop_band(self):
        self.tester.post('/new', data=dict(bandname="test_troop1", capspec="Marksman", capskill="Pierce", capweap="Blaster",
                                           troops=json.dumps(['Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey'])))
        self.tester.post('/edit/test_troop1', data=dict(bandname="test_troop1", capspec="Marksman",
                                                                  capskill=json.dumps(["Pierce"]), capweap="Blaster",
                                                                  troops=json.dumps(['Empty', 'Empty', 'Empty', 'Empty',
                                                                                     'Empty', 'Empty', 'Empty',
                                                                                     'Empty']),
                                                                  capmove="5", capfight="2",
                                                                  capshoot="2", capshield="12", capmorale="4",
                                                                  caphealth="12", capexperience="100"))
        response = self.tester.get('/edit/test_troop1')
        self.assertIn(b'315', response.data)  # Checks that the price of the troops is not returned to the bank.
        os.remove('bands/test_troop1')

    def test_change_weapon_created_troop_band(self):
        self.tester.post('/new', data=dict(bandname="test_troop2", capspec="Marksman", capskill="Pierce", capweap="Blaster",
                                           troops=json.dumps([])))
        tempResponse = self.tester.post('/edit/test_troop2', data=dict(bandname="test_troop2", capspec="Marksman",
                                                                  capskill=json.dumps(['Pierce']), capweap="Cannon",
                                                                  troops=json.dumps([]),
                                                                  capmove="5", capfight="2",
                                                                  capshoot="2", capshield="12", capmorale="4",
                                                                  caphealth="12", capexperience="100"))
        self.assertTrue(tempResponse.status_code, 200)
        response = self.tester.get('/edit/test_troop2')
        self.assertIn(b'485', response.data)  
        # Checks that the price of the previous weapon is returned to the bank, and the price of the new one added to it.
        os.remove('bands/test_troop2')

    def test_change_capexperience_created_troop_band(self):
        self.tester.post('/new', data=dict(bandname="test_troop3", capspec="Marksman", capskill="Pierce", capweap="Blaster",
                                           troops=json.dumps(['Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey'])))
        response = self.tester.post('/edit/test_troop3', data=dict(bandname="test_troop3", capspec="Marksman",
                                                                  capskill=json.dumps(["Pierce"]), capweap="Blaster",
                                                                  troops=json.dumps(['Empty', 'Empty', 'Empty', 'Empty',
                                                                                     'Empty', 'Empty', 'Empty',
                                                                                     'Empty']),
                                                                  capmove="5", capfight="2",
                                                                  capshoot="2", capshield="12", capmorale="4",
                                                                  caphealth="12", capexperience="-10"))
        self.assertTrue(response.status_code, 400)  # Checks that negative capexperience is caught.
        os.remove('bands/test_troop3')

    def test_change_capfightandshoot_created_troop_band(self):
        self.tester.post('/new', data=dict(bandname="test_troop4", capspec="Marksman", capskill="Pierce", capweap="Blaster",
                                           troops=json.dumps(['Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey'])))
        response = self.tester.post('/edit/test_troop4', data=dict(bandname="test_troop4", capspec="Marksman",
                                                                  capskill=json.dumps(['Pierce']), capweap="Blaster",
                                                                  troops=json.dumps(['Empty', 'Empty', 'Empty', 'Empty',
                                                                                     'Empty', 'Empty', 'Empty',
                                                                                     'Empty']),
                                                                  capmove="5", capfight="3",
                                                                  capshoot="3", capshield="12", capmorale="4",
                                                                  caphealth="12", capexperience="100"))
        self.assertTrue(response.status_code, 400)  # Checks that two stats changes are caught.
        os.remove('bands/test_troop4')

    def test_change_illegalspec_created_troop_band(self):
        self.tester.post('/new', data=dict(bandname="test_troop5", capspec="Marksman", capskill="Pierce", capweap="Blaster",
                                           troops=json.dumps(['Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey'])))
        response = self.tester.post('/edit/test_troop5', data=dict(bandname="test_troop5", capspec="Marksman",
                                                                  capskill=json.dumps(["Pierce"]), capweap="Blaster",
                                                                  troops=json.dumps(['Empty', 'Empty', 'Empty', 'Empty',
                                                                                     'Empty', 'Empty', 'Empty',
                                                                                     'Empty']),
                                                                  capmove="5", capfight="2",
                                                                  capshoot="30", capshield="12", capmorale="4",
                                                                  caphealth="12", capexperience="100"))
        self.assertTrue(response.status_code, 400)  # Checks that a stat is changed by more than one.
        os.remove('bands/test_troop5')

    def test_change_nonnumeric_created_troop_band(self):
        self.tester.post('/new', data=dict(bandname="test_troop6", capspec="Marksman", capskill="Pierce", capweap="Blaster",
                                           troops=json.dumps(['Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey',
                                                              'Lackey', 'Lackey', 'Lackey'])))
        response = self.tester.post('/edit/test_troop6', data=dict(bandname="test_troop6", capspec="Marksman",
                                                                  capskill=json.dumps(["Pierce"]), capweap="Blaster",
                                                                  troops=json.dumps(['Empty', 'Empty', 'Empty', 'Empty',
                                                                                     'Empty', 'Empty', 'Empty',
                                                                                     'Empty']),
                                                                  capmove="&", capfight="2",
                                                                  capshoot="2", capshield="12", capmorale="4",
                                                                  caphealth="12", capexperience="100"))
        self.assertTrue(response.status_code, 400)  # Checks that nonnumeric values are caught properly.
        os.remove('bands/test_troop6')


if __name__ == '__main__':
    unittest.main()
