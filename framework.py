import httpcodes
import json
import os
import pickle
from flask import Flask, request, render_template

# Create exportable app
app = Flask(__name__)
app.troops = {'Augment Gorilla': {'Move': 8, 'Fight': 3, 'Shoot': 0, 'Shield': 10, 'Morale': 2, 'Health': 8, 'Cost': 20,
                                  'Notes': 'Animal, Cannot carry treasure or items'},
              'Lackey': {'Move': 6, 'Fight': 2, 'Shoot': 0, 'Shield': 10, 'Morale': -1, 'Health': 10, 'Cost': 20,
                         'Notes': 'Melee Weapon'},
              'Security': {'Move': 6, 'Fight': 2, 'Shoot': 1, 'Shield': 12, 'Morale': 2, 'Health': 12, 'Cost': 80,
                           'Notes': 'Blaster, Blade'},
              'Engineer': {'Move': 4, 'Fight': 0, 'Shoot': 3, 'Shield': 12, 'Morale': 2, 'Health': 10, 'Cost': 60,
                           'Notes': 'Blaster, Repair Kit'},
              'Medic': {'Move': 5, 'Fight': 0, 'Shoot': 0, 'Shield': 12, 'Morale': 3, 'Health': 10, 'Cost': 50,
                        'Notes': 'Blade, Medkit'},
              'Commando': {'Move': 8, 'Fight': 4, 'Shoot': 0, 'Shield': 10, 'Morale': 4, 'Health': 12, 'Cost': 100,
                           'Notes': 'Stealth Suit, Blade, Needle Gun'},
              'Combat Droid': {'Move': 3, 'Fight': 2, 'Shoot': 4, 'Shield': 14, 'Morale': 0, 'Health': 14, 'Cost': 150,
                               'Notes': 'Mechanoid, Dual Blaster, Claws'},
              }
app.captain = {
    'Captain': {'Move': 5, 'Fight': 2, 'Shoot': 2, 'Shield': 12, 'Morale': 4, 'Health': 12, 'Cost': 0, 'Skillset': [],
                'Specialism': None, 'Items': [], 'Experience': 0}}
app.ensign = {'Ensign': {'Move': 7, 'Fight': 0, 'Shoot': -1, 'Shield': 10, 'Morale': 2, 'Health': 8, 'Skillset': [],
                         'Specialism': None, 'Cost': 250, 'Items': [], 'Experience': 0}}
app.specialisms = ['Engineering', 'Psychology', 'Marksman', 'Tactics', 'Melee', 'Defence']
app.skillsets = {'Engineering': ['Repair', 'Sabotage', 'Augment'], 'Psychology': ['Bolster', 'Terror', 'Counter'],
                 'Marksman': ['Aim', 'Pierce', 'Reload'], 'Tactics': ['Squad', 'Ambush', 'Surround'],
                 'Melee': ['Block', 'Risposte', 'Dual'], 'Defence': ['Shield', 'Sacrifice', 'Resolute']}
app.weapon = ['Blaster', 'Needle Gun', 'Blade', 'Cannon', 'Whip']
app.cost = {'Blaster': 5, 'Needle Gun': 12, 'Blade': 3, 'Cannon': 15, 'Whip': 5}

statMax = {'Captain': {'Fight': 10, 'Shoot': 10, 'Morale': 12, 'Health': 20},
           'Ensign': {'Fight': 6, 'Shoot': 5, 'Morale': 6, 'Health': 16}}


@app.route('/', methods=['GET'])
def welcome_page():
    return app.send_static_file('index.html'), httpcodes.OK


def sumband(createdband):
    """
    Only used when creating a new warband.
    :return: cost of creating the input warband.
    """
    total = 0

    for item in createdband['Captain']['Items']:
        total += app.cost[item]

    if 'Ensign' in createdband.keys():
        for item in createdband['Ensign']['Items']:
            total += app.cost[item]

    for troop in createdband['Troops']:
        total += app.troops[troop]['Cost']

    if 'Ensign' in createdband:
        total += 250

    return total


def hiringCosts(band):
    """
    :return: cost of hiring the desired troops,
    """
    hiringCosts = [0, 0, 0, 0, 0, 0, 0]
    for troop in band['Troops']:
        if troop == 'Augment Gorilla':
            hiringCosts[0] += app.troops[troop]['Cost']
        elif troop == 'Lackey':
            hiringCosts[1] += app.troops[troop]['Cost']
        elif troop == 'Security':
            hiringCosts[2] += app.troops[troop]['Cost']
        elif troop == 'Engineer':
            hiringCosts[3] += app.troops[troop]['Cost']
        elif troop == 'Medic':
            hiringCosts[4] += app.troops[troop]['Cost']
        elif troop == 'Commando':
            hiringCosts[5] += app.troops[troop]['Cost']
        elif troop == 'Combat Droid':
            hiringCosts[6] += app.troops[troop]['Cost']

    return hiringCosts


def validate_newband(newband):
    """
    Only used for creating a new warand.
    :return: two element list with [remaining_credits, 0 or error page]
    """
    members = len(newband['Troops']) + 1  # troops plus Captain.
    if 'Ensign' in newband:
        members += 1  # troops, Captain and Ensign.

    newband['Treasury'] = 500

    if newband["Name"] == "":
        # Catches attempt to create band without name.
        return [newband['Treasury'], 'E_incomplete.html']  # To show 'E_incomplete.html' error page.

    if os.path.isfile(str("bands/"+newband["Name"])):
        # Catches attempt to create band with previously used name.
        return [newband['Treasury'], 'E_duplicateBand.html']  # To show 'E_duplicateBand.html' error page.

    if (newband['Captain']['Items'] == ['Empty']) or (newband['Captain']['Skillset'] == ['Empty']):
        # Catches attempt to create band without Captain Weapon on Skills
        return [newband['Treasury'], 'E_incomplete.html']  # To show 'E_incomplete.html' error page.
    if 'Ensign' in newband:
        if (newband['Ensign']['Items'] == ['Empty']) or (newband['Ensign']['Skillset'] == ['Empty']):
            # Catches attempt to create band without Ensign Weapon on Skills
            return [newband['Treasury'], 'E_incomplete.html']  # To show 'E_incomplete.html' error page.

    newband['Treasury'] -= sumband(newband)

    if members > 10:
        # Catches attempt to create band with more than 10 members.
        return [newband['Treasury'], 'E_troopNumber.html']  # To show 'E_troopNumber.html' error page.
    elif newband['Treasury'] < 0:
        # Catches attempt to create band with negative remaining credits.
        return [newband['Treasury'], 'E_negativeTreasury.html']  # To show 'E_negativeTreasury.html' error page.
    else:
        return [newband['Treasury'], 0]


def validate_skills(oldband, experience, specialism, proposedSkills, isEnsign):
    """
    Only used for editing a band.
    :return: three element list with [experience, checked skills, 0 or error page]
    """
    member = 'Captain'
    if isEnsign:
        member = 'Ensign'

    try:
        # Checks that the input is numeric.
        xp = int(experience)
    except ValueError:
        return [0, proposedSkills, 'E_nonNumeric.html']

    try:
        # This is necessary in case the edited band wants to hire an Ensign.
        oldSkills = oldband[member]['Skillset']
    except KeyError:
        oldSkills = []

    skills = app.skillsets[specialism]

    oldSkillArr = {skills[0]: 0, skills[1]: 0, skills[2]: 0}
    newskillArr = {skills[0]: 0, skills[1]: 0, skills[2]: 0}

    for skill in skills:
        for proposedSkill in proposedSkills:
            if proposedSkill == skill:
                newskillArr[skill] += 1
                if newskillArr[skill] > 1:
                    return [xp, proposedSkills, 'E_invalidSkillChange.html']
        for oldSkill in oldSkills:
            if oldSkill == skill:
                oldSkillArr[skill] += 1
                if oldSkillArr[skill] > 1:
                    oldSkillArr[skill] = 1

    for skill in skills:
        if newskillArr[skill] < oldSkillArr[skill]:
            # Catches attempt to unlearn a skill.
            return [xp, proposedSkills, 'E_unlearnSkill.html']
        if newskillArr[skill] > oldSkillArr[skill]:
            # Learning a skill costs 10 Experience.
            xp -= 10

    if xp < 0:
        # Catches attempt to create band with negative Experience.
        return [xp, proposedSkills, 'E_insufficientExperience.html']
    else:
        return [xp, proposedSkills, 0]


def validate_band(oldband, newband):
    """
    Only used for editing a band.
    :return: two element list with [remaining credits, 0 or error page]
    """
    members = len(newband['Troops']) + 1  # troops plus Captain.
    itemChangeCost = 0

    if 'Ensign' in newband:
        members += 1  # troops, Captain and Ensign.
        if 'Ensign' not in oldband:
            # For adding an Ensign in the edit band stage.
            itemChangeCost -= 250 + app.cost[newband['Ensign']['Items'][0]]
        else:
            # Check Ensign's weapon cost change.
            if newband['Ensign']['Items'] != oldband['Ensign']['Items']:  # checks change in items to account cost change.
                itemChangeCost += (app.cost[oldband['Ensign']['Items'][0]] - app.cost[newband['Ensign']['Items'][0]])

    if newband['Captain']['Items'] != oldband['Captain']['Items']:
        # Check Ensign's weapon cost change.
        itemChangeCost += app.cost[oldband['Captain']['Items'][0]] - app.cost[newband['Captain']['Items'][0]]

    oldHiringCost = hiringCosts(oldband)
    newHiringCost = hiringCosts(newband)
    hiringCost = 0

    for i in range(len(oldHiringCost)):
        # Checks for added/removed troops.
        hiringCost += oldHiringCost[i] - newHiringCost[i]

    if hiringCost < 0:
        newband['Treasury'] = oldband['Treasury'] + hiringCost + itemChangeCost
    else:
        # Removing troops does not return money.
        newband['Treasury'] = oldband['Treasury'] + itemChangeCost

    if members > 10:
        # Catches attempt to create band with more than 10 members.
        return [newband['Treasury'], 'E_troopNumber.html']  # Convention to show 'E_troopNumber.html' error page.
    elif newband['Treasury'] < 0:
        # Catches attempt to create band with negative remaining credits.
        return [newband['Treasury'],
                'E_negativeTreasury.html']  # Convention to show 'E_negativeTreasury.html' error page.
    else:
        return [newband['Treasury'], 0]


def validate_stat(oldband, experience, stat_name, stat_value, isEnsign):
    """
    Only used for editing a band.
    :return: three element list with [experience, stat, 0 or error page]
    """
    member = 'Captain'
    if isEnsign:
        member = 'Ensign'

    try:
        # Checks that the input is numeric (this should already be checked in validate_all_stats).
        newExp = int(experience)
    except ValueError:
        return [0, stat_value, 'E_nonNumeric.html']

    oldStat = int(oldband[member][stat_name])
    maxStat = int(statMax[member][stat_name]) # get the maximum possible value for stat_name.

    if oldStat == int(stat_value):
        return [newExp, oldStat, 0]
    else:
        if newExp < 10:
            return [newExp, oldStat, 'E_insufficientExperience.html']
        elif oldStat > int(stat_value):
            return [newExp, oldStat, 'E_invalidStatDecrease.html']
        elif int(stat_value) - oldStat > 1:
            # Changes of more than one point per stat are not allowed under game rules.
            return [newExp, oldStat, 'E_invalidStatChange.html']
        elif int(stat_value) > maxStat:
            return [newExp, oldStat, 'E_invalidStatMax.html']
        else:
            return [newExp - 10, oldStat + 1, 0]


def validate_all_stats(oldband, experience, stat_values, isEnsign):
    """
    Only used for editing a band.
    :return: three element list with [experience, [fight, shoot, morale, health], 0 or error page]
    """
    member = 'Captain'
    if isEnsign:
        member = 'Ensign'

    stats_name = ['Fight', 'Shoot', 'Morale', 'Health']  # The only stats that can be changed.

    oldStats = []
    try:
        # Checks that the input is numeric.
        newExp = int(experience)
        newStats = [int(stat) for stat in stat_values]
    except ValueError:
        return [0, stat_values, 'E_nonNumeric.html']

    try:
        # For adding an Ensign in the edit band stage.
        for i in range(len(stats_name)):
            oldStats.append(int(oldband[member][stats_name[i]]))
    except KeyError:
        return [newExp, stat_values, 0]

    statChanges = 0
    for i in range(len(stats_name)):
        if oldStats[i] != newStats[i]:
            statChanges += 1

    if newExp < 0:
        return [newExp, newStats, 'E_negativeExperience.html']
    elif newExp < 10:
        if statChanges > 0:
            return [newExp, oldStats, 'E_insufficientExperience.html']
        else:
            return [newExp, oldStats, 0]

    goodStats = []
    for i in range(len(stats_name)):
        newStat = validate_stat(oldband, newExp, stats_name[i], newStats[i], isEnsign)
        if newStat[2] != 0:
            # Propagates error file from validate_stat(...)
            return [newExp, newStats, newStat[2]]
        else:
            newExp = newStat[0]
            goodStats.append(newStat[1])

    return [newExp, goodStats, 0]


@app.route('/new', methods=['GET', 'POST'])
def new_warband():
    if request.method == 'GET':
        return render_template('blankband.html', people=app.troops, captain=app.captain, ensign=app.ensign,
                               specs=app.specialisms, skills=app.skillsets, weaps=app.weapon), httpcodes.OK
    if request.method == 'POST':
        bandname = request.form['bandname']
        capspec = request.form['capspec']
        capskill = request.form['capskill']
        capweap = request.form['capweap']
        troops = json.loads(request.form['troops'])
        createdband = dict()
        createdband['Name'] = bandname
        createdband['Captain'] = dict(app.captain['Captain'])
        createdband['Captain']['Specialism'] = capspec
        createdband['Captain']['Skillset'].append(capskill)
        createdband['Captain']['Items'] = [capweap]

        if 'hasensign' in request.form.keys():
            ensspec = request.form['ensspec']
            ensskill = request.form['ensskill']
            ensweap = request.form['ensweap']
            createdband['Ensign'] = dict(app.ensign['Ensign'])
            createdband['Ensign']['Specialism'] = ensspec
            createdband['Ensign']['Skillset'].append(ensskill)
            createdband['Ensign']['Items'] = [ensweap]
        createdband['Troops'] = []

        for item in troops:
            if item != "Empty":
                createdband['Troops'].append(item)

        validate = validate_newband(createdband)
        if validate[1] != 0:
            # Propagates error file from validate_newband(...)
            return render_template(validate[1]), httpcodes.BAD_REQUEST
        else:
            createdband['Treasury'] = validate[0]

        bandfile = open(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands"),
                                     createdband['Name']), "wb")
        # Creates createdband file.
        pickle.dump(createdband, bandfile)
        bandfile.close()

        return render_template('success.html'), httpcodes.CREATED


@app.route('/edit', methods=['GET'])
def edit_warband():
    if os.path.isdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands")):
        bands = os.listdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands"))
    else:
        os.mkdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands"))
        bands = None
    if request.method == 'GET':
        return render_template('bandlist.html', bands=bands), httpcodes.OK


@app.route('/edit/<band>', methods=['GET', 'POST'])
def edit_given_warband(band):
    bandfile = open(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands"), band), "rb")
    loadedband = pickle.load(bandfile)
    bandfile.close()
    if request.method == 'GET':
        return render_template('editband.html', band=loadedband, people=app.troops, captain=app.captain,
                               ensign=app.ensign, specs=app.specialisms, skills=app.skillsets,
                               weaps=app.weapon), httpcodes.OK
    if request.method == 'POST':

        bandname = request.form['bandname']
        capspec = request.form['capspec']
        capskill = request.form['capskill']
        skills = json.loads(capskill)
        capweap = request.form['capweap']
        troops = json.loads(request.form['troops'])
        capmove = request.form['capmove']
        capfight = request.form['capfight']
        capshoot = request.form['capshoot']
        capshield = request.form['capshield']
        capmorale = request.form['capmorale']
        caphealth = request.form['caphealth']
        capexperience = request.form['capexperience']
        createdband = dict()
        createdband['Name'] = bandname
        createdband['Captain'] = dict(app.captain['Captain'])
        createdband['Captain']['Specialism'] = capspec

        proposedSkills = skills
        validSkills = validate_skills(loadedband, capexperience, capspec, proposedSkills, False)
        if validSkills[2] != 0:
            # Propagates error file from validate_skills(...)
            return render_template(validSkills[2]), httpcodes.BAD_REQUEST
        else:
            createdband['Captain']['Experience'] = validSkills[0]
            createdband['Captain']['Skillset'] = validSkills[1]

        createdband['Captain']['Items'] = [capweap]
        capexperience = createdband['Captain']['Experience']

        proposedStats = [capfight, capshoot, capmorale, caphealth]
        validStats = validate_all_stats(loadedband, capexperience, proposedStats, False)

        if validStats[2] != 0:
            # Propagates error file from validate_all_stats(...)
            return render_template(validStats[2]), httpcodes.BAD_REQUEST
        else:
            createdband['Captain']['Experience'] = validStats[0]
            createdband['Captain']['Fight'] = validStats[1][0]
            createdband['Captain']['Shoot'] = validStats[1][1]
            createdband['Captain']['Morale'] = validStats[1][2]
            createdband['Captain']['Health'] = validStats[1][3]

        createdband['Captain']['Move'] = capmove
        createdband['Captain']['Shield'] = capshield

        if 'hasensign' in request.form.keys():
            ensspec = request.form['ensspec']
            ensskill = request.form['ensskill']
            enskills = json.loads(ensskill)
            ensmove = request.form['ensmove']
            ensshield = request.form['ensshield']
            ensexperience = request.form['ensexperience']
            ensweap = request.form['ensweap']
            createdband['Ensign'] = dict(app.ensign['Ensign'])
            createdband['Ensign']['Specialism'] = ensspec

            proposedSkills = enskills
            validSkills = validate_skills(loadedband, ensexperience, ensspec, proposedSkills, True)
            if validSkills[2] != 0:
                # Propagates error file from validate_skills(...)
                return render_template(validSkills[2]), httpcodes.BAD_REQUEST
            else:
                createdband['Ensign']['Experience'] = validSkills[0]
                createdband['Ensign']['Skillset'] = validSkills[1]

            createdband['Ensign']['Items'] = [ensweap]
            ensexperience = createdband['Ensign']['Experience']

            proposedStats = [request.form['ensfight'], request.form['ensshoot'], request.form['ensmorale'],
                             request.form['enshealth']]
            validStats = validate_all_stats(loadedband, ensexperience, proposedStats, True)

            if validStats[2] != 0:
                # Propagates error file from validate_all_stats(...)
                return render_template(validStats[2]), httpcodes.BAD_REQUEST
            else:
                createdband['Ensign']['Experience'] = validStats[0]
                createdband['Ensign']['Fight'] = validStats[1][0]
                createdband['Ensign']['Shoot'] = validStats[1][1]
                createdband['Ensign']['Morale'] = validStats[1][2]
                createdband['Ensign']['Health'] = validStats[1][3]

            createdband['Ensign']['Move'] = ensmove
            createdband['Ensign']['Shield'] = ensshield

        createdband['Troops'] = []
        for item in troops:
            if item != "Empty":
                createdband['Troops'].append(item)

        validate = validate_band(loadedband, createdband)
        if validate[1] != 0:
            # Propagates error file from validate_band(...)
            return render_template(validate[1]), httpcodes.BAD_REQUEST
        else:
            createdband['Treasury'] = validate[0]

        # Saves createdband file with new info.
        pickle.dump(createdband,
                    open(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands"), bandname),
                         "wb"))
        return render_template('success.html'), httpcodes.OK


@app.route('/delete/<band>', methods=['GET'])
def delete_given_warband(band):
    os.remove(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands", band))
    if os.path.isdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands")):
        bands = os.listdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands"))
    else:
        os.mkdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), "bands"))
        bands = None
    if request.method == 'GET':
        return render_template('bandlist.html', bands=bands), httpcodes.OK


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
